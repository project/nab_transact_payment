<?php
/**
 * Contains NabTransactPaymentMethodController.
 */

/**
 * A NAB Transact hosted payment method.
 */
class NabTransactHostedPaymentMethodController extends PaymentMethodController {

  /**
   * The production server URL.
   */
  const LIVE_SERVER_URL = 'https://transact.nab.com.au/live/hpp/payment';

  /**
   * The test server URL.
   */
  const TEST_SERVER_URL = 'https://transact.nab.com.au/test/hpp/payment';

  public $controller_data_defaults = array(
    'mode'             => 'test',
    'vendor_name'      => '',
    'payment_alert'    => '',
    'refund_policy'    => '',
    'privacy_policy'   => '',
    'gst_rate'         => '10',
    'gst_added'        => 'true',
    'return_link_text' => '',
  );

  public $payment_method_configuration_form_elements_callback = 'nab_transact_hosted_payment_payment_method_configuration_form_elements';

  function __construct() {
    $currency_codes = array('AUD');
    $this->currencies = array_fill_keys($currency_codes, array());
    $this->title = 'NAB Transact Hosted Payment Page';
  }

  /**
   * Implements PaymentMethodController::validate().
   */
  function validate(Payment $payment, PaymentMethod $payment_method, $strict) {
  }

  /**
   * Implements PaymentMethodController::execute().
   */
  function execute(Payment $payment) {
    entity_save('payment', $payment);
    $_SESSION['nab_transact_hosted_payment_pid'] = $payment->pid;
    drupal_goto('nab_transact_hosted_payment/redirect/' . $payment->pid);
  }

  /**
   * Hashes a Payment PID.
   *
   * @param integer $pid
   *
   * @return string
   */
  static function hashPID($pid) {
    return hash('sha256', $pid . drupal_get_hash_salt());
  }

  /**
   * Creates an invoice ID from a Payment PID.
   *
   * @see NabTransactHostedPaymentMethodController::PID()
   *
   * @param integer $pid
   *
   * @return string
   */
  static function invoiceID($pid) {
    return 'tc-' . self::hashPID($pid) . '-' . $pid;
  }

  /**
   * Extracts a Payment PID from a PayPa invoice ID.
   *
   * @see NabTransactHostedPaymentMethodController::invoiceID()
   *
   * @param string $invoice_id
   *
   * @return integer|false
   *   The PID, or FALSE if the invoice ID did not contain a valid PID.
   */
  static function PID($invoice_id) {
    $fragments = explode('-', $invoice_id);
    if (count($fragments) == 3) {
      list(, $hash, $pid) = $fragments;
      return $hash == self::hashPID($pid) ? (int) $pid : FALSE;
    }
    return FALSE;
  }

}
