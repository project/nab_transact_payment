<?php

/**
 * Contains class NabTransactIPN.
 *
 * $data['bank_reference']
 * $data['payment_amount']
 * $data['payment_number']
 */

/**
 * A NAB Transact IPN.
 *
 * @see NabTransactIPNController
 */
class NabTransactIPN {

  /**
   * The PID of the Payment this IPN is for.
   *
   * @see NabTransactHostedPaymentController::invoiceID()
   * @see NabTransactHostedPaymentController::PID()
   *
   * @var string
   */
  public $pid = 0;

  /**
   * The NAB transaction ID.
   *
   * @var string
   */
  public $bank_reference = '';

  /**
   * Constructor.
   */
  function __construct(array $properties = array()) {
    foreach ($properties as $property => $value) {
      $this->$property = $value;
    }
  }
}
