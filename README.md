NAB Transact Hosted Payments
----------------------------

This module provides supported for hosted NAB transact payments using the
payments module.

That means this is *not* a module for Drupal Commerce, unles you want to use
the Payments API module in Commerce.
