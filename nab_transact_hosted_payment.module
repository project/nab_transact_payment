<?php

/**
 * @file
 * Contains hook implementations and global functions.
 */

/**
 * Implements hook_menu().
 */
function nab_transact_hosted_payment_menu() {
  $items['nab_transact_hosted_payment/redirect/%entity_object'] = array(
    'load arguments' => array('payment'),
    'title' => 'Go to payment server',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('nab_transact_hosted_payment_form_redirect', 2),
    'access callback' => 'nab_transact_hosted_payment_form_redirect_access',
    'access arguments' => array(2),
    'type' => MENU_CALLBACK,
  );
  $items['nab_transact_hosted_payment/return'] = array(
    'title' => 'NAB Transact Hosted return url',
    'page callback' => 'nab_transact_hosted_payment_return',
    'access callback' => 'nab_transact_hosted_payment_return_access',
    'type' => MENU_CALLBACK,
  );
  $items['nab_transact_hosted_payment/return/cancel/%entity_object/%'] = array(
    'load arguments' => array('payment'),
    'title' => 'NAB Transact Hosted return url',
    'page callback' => 'nab_transact_hosted_payment_return_cancel',
    'page arguments' => array(3, 4),
    'access callback' => 'nab_transact_hosted_payment_return_cancel_access',
    'access arguments' => array(3, 4),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_payment_method_controller_info().
 */
function nab_transact_hosted_payment_payment_method_controller_info() {
  return array('NabTransactHostedPaymentMethodController');
}

/**
 * Implements hook_entity_load().
 */
function nab_transact_hosted_payment_entity_load(array $entities, $entity_type) {
  if ($entity_type == 'payment_method') {
    $pmids = array();
    foreach ($entities as $payment_method) {
      if ($payment_method->controller->name == 'NabTransactHostedPaymentMethodController') {
        $pmids[] = $payment_method->pmid;
      }
    }
    if ($pmids) {
      $query = db_select('nab_transact_hosted_payment_method')
        ->fields('nab_transact_hosted_payment_method')
        ->condition('pmid', $pmids);
      $result = $query->execute();
      while ($data = $result->fetchAssoc()) {
        $payment_method = $entities[$data['pmid']];
        $payment_method->controller_data = (array) $data;
        unset($payment_method->controller_data['pmid']);
      }
    }
  }
}

/**
 * Implements hook_ENTITY_TYPE_ACTION().
 */
function nab_transact_hosted_payment_payment_method_insert(PaymentMethod $payment_method) {
  if ($payment_method->controller->name == 'NabTransactHostedPaymentMethodController') {
    $values = $payment_method->controller_data += $payment_method->controller->controller_data_defaults;
    $values['pmid'] = $payment_method->pmid;
    drupal_write_record('nab_transact_hosted_payment_method', $values);
  }
}

/**
 * Implements hook_ENTITY_TYPE_ACTION().
 */
function nab_transact_hosted_payment_payment_method_update(PaymentMethod $payment_method) {
  if ($payment_method->controller->name == 'NabTransactHostedPaymentMethodController') {
    $values = $payment_method->controller_data += $payment_method->controller->controller_data_defaults;
    $values['pmid'] = $payment_method->pmid;
    drupal_write_record('nab_transact_hosted_payment_method', $values, 'pmid');
  }
}

/**
 * Implements hook_ENTITY_TYPE_ACTION().
 */
function nab_transact_hosted_payment_payment_method_delete($entity) {
  if ($entity->controller->name == 'NabTransactHostedPaymentMethodController') {
    db_delete('nab_transact_hosted_payment_method')
      ->condition('pmid', $entity->pmid)
      ->execute();
  }
}

/**
 * Form build callback: implements
 * PaymentMethodController::payment_method_configuration_form_elements_callback.
 */
function nab_transact_hosted_payment_payment_method_configuration_form_elements(array $form, array &$form_state) {
  $payment_method = $form_state['payment_method'];
  $controller = $payment_method->controller;
  $controller_data = $payment_method->controller_data + $controller->controller_data_defaults;
  $form = array();

  $elements['mode'] = array(
    '#type' => 'radios',
    '#title' => t('Select test or live mode'),
    '#default_value' => $controller_data['mode'],
    '#options' => array('test' => t('Test'), 'live' => t('Live')),
    '#required' => TRUE,
  );
  $elements['vendor_name'] = array(
    '#type' => 'textfield',
    '#title' => t('NAB merchant ID'),
    '#default_value' => $controller_data['vendor_name'],
    '#size' => 80,
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  $elements['payment_alert'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchant email'),
    '#description' => t('Contact email for the vendor'),
    '#size' => 80,
    '#default_value' => $controller_data['payment_alert'],
    '#required' => TRUE,
  );
  $elements['refund_policy'] = array(
    '#type' => 'textfield',
    '#title' => t('Refund policy URL'),
    '#size' => 80,
    '#description' => t('The full URL to your refund policy page.'),
    '#default_value' => $controller_data['refund_policy'],
  );
  $elements['privacy_policy'] = array(
    '#type' => 'textfield',
    '#title' => t('Privacy policy URL'),
    '#size' => 80,
    '#description' => t('The full URL to your privacy policy page.'),
    '#default_value' => $controller_data['privacy_policy'],
  );
  $elements['gst_rate'] = array(
    '#type' => 'textfield',
    '#title' => t('GST rate'),
    '#size' => 5,
    '#description' => t('Set the GST rate.'),
    '#default_value' => $controller_data['gst_rate'],
  );
  $elements['gst_added'] = array(
    '#type' => 'radios',
    '#title' => t('Let NAB Transact handle GST'),
    '#default_value' => $controller_data['gst_added'],
    '#options' => array('true' => t('True'), 'false' => t('False')),
    '#required' => TRUE,
  );
  $elements['return_link_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Return link text'),
    '#size' => 80,
    '#description' => t('Title for the checkout return link.'),
    '#default_value' => $controller_data['return_link_text'],
    '#required' => TRUE,
  );

  return $elements;
}

/**
 * Implements form validate callback for
 * nab_transact_hosted_payment_payment_method_configuration_form_elements().
 */
function nab_transact_hosted_payment_payment_method_configuration_form_elements_validate(array $element, array &$form_state) {
  $values = drupal_array_get_nested_value($form_state['values'], $element['#parents']);

  $controller_data = &$form_state['payment_method']->controller_data;
  $controller_data['mode']             = $values['mode'];
  $controller_data['vendor_name']      = $values['vendor_name'];
  $controller_data['payment_alert']    = $values['payment_alert'];
  $controller_data['refund_policy']    = $values['refund_policy'];
  $controller_data['privacy_policy']   = $values['privacy_policy'];
  $controller_data['gst_rate']         = $values['gst_rate'];
  $controller_data['gst_added']        = $values['gst_added'];
  $controller_data['return_link_text'] = $values['return_link_text'];

  // Need the controller for its defines.
  $controller = $form_state['payment_method']->controller;
  $controller_data['server'] = ($controller_data['mode'] == 'test') ? $controller::TEST_SERVER_URL : $controller::LIVE_SERVER_URL;

  if (!is_numeric($values['gst_rate']) || !is_numeric($values['gst_rate'])) {
    form_error($element['gst_rate'], t('The GST rate must be positive number or 0.'));
  }

  if (!valid_url($values['refund_policy'])) {
    form_error($element['refund_policy'], t('The refund policy link is not valid.'));
  }

  if (!valid_email_address($values['payment_alert'])) {
    form_error($element['payment_alert'], t('The email address is not valid.'));
  }
}

/**
 * Form build callback: the redirect form.
 */
function nab_transact_hosted_payment_form_redirect(array $form, array &$form_state, Payment $payment) {
  global $base_url;
  global $language;

  $controller_data = $payment->method->controller_data;

  // Prepare POST data.
  $return_url = url('nab_transact_hosted_payment/return/' . $payment->pid, array('query' => array('payment_amount' => '', 'bank_reference' => '', 'payment_number' => ''), 'absolute' => TRUE));
  $data = array(
    'vendor_name' => $controller_data['vendor_name'],
    'refund_policy' => $controller_data['refund_policy'],
    'privacy_policy' => $controller_data['privacy_policy'],
    'payment_alert' => $controller_data['payment_alert'],
    'gst_rate' =>  $controller_data['gst_rate'],
    'gst_added' => $controller_data['gst_added'],
    'payment_reference' => NabTransactHostedPaymentMethodController::invoiceID($payment->pid),
    'return_link_url' => $return_url,
    'return_link_text' => $controller_data['return_link_text'],
  );

  // Calculate a total amount.
  $total = 0.00;

  // Add each line item separately. The first item starts at 1.
  $index = 1;
  foreach ($payment->line_items as $line_item) {
    // $data['amount_' . $index] = $line_item->totalAmount(TRUE);
    // $data['item_name_' . $index] = $line_item->name;
    // $data['quantity_' . $index] = $line_item->quantity;
    $total += $line_item->totalAmount(TRUE);
    $index++;
  }

  $data['order_price'] = round($total, 2);

  // Build the form.
  $form['#action'] = url($controller_data['server'], array(
    'external' => TRUE,
  ));
  foreach ($data as $name => $value) {
    if (!empty($value)) {
      $form[$name] = array(
        '#type' => 'hidden',
        '#value' => $value,
      );
    }
  }
  $form['message'] = array(
    '#type' => 'markup',
    '#markup' => '<p>' . t('You will be redirected to the NAB Transact payment server to authorise the payment.') . '</p>',
  );
  // We need form submission as quickly as possible, so use light inline code.
  $form['js'] = array(
    '#type' => 'markup',
    '#markup' => '<script type="text/javascript">document.getElementById(\'nab-transact-form-redirect\').submit();</script>',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Pay via secure credit card at Mational Australia Bank.'),
  );

  dpm($form);

  return $form;
}

/**
 * Access callback for the redirect page.
 *
 * @param Payment $payment
 *   The payment to check access to.
 * @param object $user
 *   An optional user to check access for. If NULL, then the currently logged
 *   in user is used.
 *
 * @return bool
 */
function nab_transact_hosted_payment_form_redirect_access(Payment $payment, $account = NULL) {
  global $user;

  return is_a($payment->method->controller, 'NabTransactHostedPaymentMethodController')
    && payment_status_is_or_has_ancestor($payment->getStatus()->status, PAYMENT_STATUS_PENDING)
    && isset($_SESSION['nab_transact_hosted_payment_pid']) && $_SESSION['nab_transact_hosted_payment_pid'] == $payment->pid;
}

/**
 * Return callback.
 */
function nab_transact_hosted_payment_return() {
  parse_str($_SERVER['QUERY_STRING'], $data);
  dpm($data, __FUNCTION__);
  $ipn = new NabTransactIPN($data);
  NabTransactHostedPaymentMethodController::save($ipn);
  NabTransactHostedPaymentMethodController::process($ipn);
  $payment = entity_load_single('payment', NabTransactHostedPaymentMethodController::PID($ipn['bank_reference']));
  $payment->finish();
}

/**
 * Access callback for the return URL.
 *
 * @return bool
 */
function nab_transact_hosted_payment_return_access() {
  return NabTransactHostedPaymentMethodController::validate($_SERVER['QUERY_STRING']);
}

/**
 * Cancellation return callback.
 *
 * @return Payment
 *
 * @return NULL
 */
function nab_transact_hosted_payment_return_cancel(Payment $payment) {
  $payment->setStatus(new PaymentStatusItem(PAYMENT_STATUS_CANCELLED));
  $payment->finish();
}

/**
 * Access callback for the cancellation return URL.
 *
 * @param Payment $payment
 *   The Payment to check access to.
 * @param string $hash
 *   The hash based on $payment->pid.
 *
 * @return bool
 */
function nab_transact_hosted_payment_return_cancel_access(Payment $payment, $hash) {
  return NabTransactHostedPaymentMethodController::hashPID($payment->pid) == $hash;
}
